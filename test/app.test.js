const test = require('tape')
 const supertest = require('supertest')
 const app = require('../app')


 test('Aplicar Desconto 10-5', (t) => {
    supertest(app)
    .get('/aplicarDesconto/10/5')
    .expect('Content-Type', /json/)
    .expect(200)
    .end((err, res) =>{
    t.error(err, 'Sem erros -  Resultado 5')
    t.assert(res.body.valorDescontado === 5, "Desconto correto")
    t.end()
    })
    })


    test('Aplicar Desconto 20-13', (t) => {
      supertest(app)
      .get('/aplicarDesconto/20/13')
      .expect('Content-Type', /json/)
      .expect(200)
      .end((err, res) =>{
      t.error(err, 'Sem erros - Resultado 7')
      t.assert(res.body.valorDescontado === 7, "Desconto correto")
      t.end()
      })
      })

      test('Aplicar Desconto 300-10', (t) => {
         supertest(app)
         .get('/aplicarDesconto/300/10')
         .expect('Content-Type', /json/)
         .expect(200)
         .end((err, res) =>{
         t.error(err, 'Sem erros - Resultado 290')
         t.assert(res.body.valorDescontado === 290, "Desconto correto")
         t.end()
         })
         })